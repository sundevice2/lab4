const mongoose = require('mongoose')

const ScoresSchema = new mongoose.Schema({
    user_id: { type: mongoose.mongo.ObjectId, ref: "users" },
    course_id: { type: mongoose.mongo.ObjectId, ref: "courses" },
    score: { type: Number, required: true },
    ScoreDate: { type: Date },
    img: String
});

const Score = mongoose.model('scores', ScoresSchema);

module.exports = Score;