const UserModel = require('../models/user.js');


class UserRepository {
    constructor() {
    }
    async getUsers() {
        const userDocs = await UserModel.find({});
        const usersArr = userDocs.map(i => i.toJSON());
        return usersArr;
    }
    async getUserById(id) {
        const user = await UserModel.findById(id);
        return user;
    }
}


module.exports = UserRepository;