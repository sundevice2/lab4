const Course = require('../models/course.js');

class CourseRepository {

    async getCourses() {
        const courses = await Course.find();
        return courses;
    }

    async getCourseById(id) {
        const course = await Course.findById(id);
        return course;
    }

    async addCourse(courseModel) {
        const course = new Course(courseModel);
        const newCourse = await course.save();
        return newCourse.id;
    }

    async updateCourse(id, updatedInfo) {
        const updatedCourse = await Course.updateOne({ _id: id }, {
            $set: {
                name: updatedInfo.name,
                duration: updatedInfo.duration,
                group: updatedInfo.group,
                credits: updatedInfo.credits,
                date: updatedInfo.date
            }
        });
        return updatedCourse;
    }

    async updateCoursePhoto(id, img) {
        const updatedCourse = await Course.updateOne({ _id: id }, {
            $set: {
                img: img
            }
        });
        return updatedCourse;
    }

    async deleteCourse(id) {
        const deletedCourse = await Course.deleteOne({ _id: id });
        return deletedCourse;
    }
}

module.exports = CourseRepository;