const CourseRepository = require('../repositories/courseRepository')
const courseRepository = new CourseRepository();

const cloudinary = require('../cloudinary.js')

module.exports = {
    async getCourseById(req, res) {
        try {
            const id = req.params.id;
            const course = await courseRepository.getCourseById(id);
            if (course) {
                res.status(200).render('course', {
                    headTitle: 'Courses', coursesCurrent:
                        'current', course: course
                });
            }
            else {
                res.status(404).render('course', {
                    headTitle: 'Courses', coursesCurrent: 'current',
                    course: null, msg: '404! No such course.'
                });
            }
        }
        catch (e) {
            console.log(e);
            res.status(500).render('course', {
                headTitle: 'Courses', coursesCurrent: 'current',
                course: null, msg: 'Server error!'
            });
        }
    },

    async getCourses(req, res) {
        try {
            const strPage = req.query.page;
            const per_page = 4;
            let page;
            if (strPage === undefined) page = 1;
            else page = parseInt(strPage);
            if (page < 1 || isNaN(page)) page = 1;

            let courses = await courseRepository.getCourses();
            const nameSearch = req.query.name;

            if (courses) {
                if (!(nameSearch === undefined)) {
                    courses = courses.filter(item => item.name.includes(nameSearch));
                    nameQuery = '&name=' + nameSearch;
                }

                const size = courses.length;
                const offset = per_page * (page - 1);
                let maxPage = Math.ceil(size / per_page);
                const coursesPage = courses.slice(offset, offset + per_page);
                const leftPage = [];
                const rightPage = [];

                if (page === 1) leftPage.push({ curPage: page });
                else leftPage.push({ curPage: page, page: page - 1 });

                if (maxPage === 0 || size === 0) {
                    maxPage = 1;
                    page = 1;
                }
                else if (page > maxPage) page = maxPage;

                if (offset + per_page < size)
                    rightPage.push({ curPage: page, page: page + 1 });
                else rightPage.push({ curPage: page });

                const params = {
                    headTitle: 'Courses', coursesCurrent: 'current',
                    coursesPage: coursesPage, leftPage: leftPage,
                    rightPage: rightPage, nameQuery: nameSearch,
                    maxPage: maxPage
                };
                res.status(200).render('courses', params);
            }
            else {
                res.status(200).render('courses', {
                    headTitle: 'Courses', coursesPage: null,
                    coursesCurrent: 'current', nameQuery: nameSearch
                });
            }
        }
        catch (e) {
            console.log(e);
            res.status(500).render('course', {
                headTitle: 'Courses', coursesCurrent: 'current',
                course: null, msg: 'Server error!'
            });
        }
    },

    async addCourse(req, res) {
        const media = req.files.course_img;
        try {
            if (media) {
                const img = await cloudinary.uploadRaw(media.data);
                req.body.img = img.url;
            }
            else {
                req.body.img = null;
            }
            const id = await courseRepository.addCourse(req.body);
            res.status(303).redirect('/courses/' + id);
        }
        catch (e) {
            console.log(e);
            res.status(500).render('course', {
                headTitle: 'Courses', coursesCurrent: 'current',
                course: null, msg: 'Server error!'
            });
        }
    },

    async getAndUpdateCourse(req, res) {
        try {
            const id = req.params.id;
            const course = await courseRepository.getCourseById(id);
            if (course) {
                res.status(200).render('updateCourse', {
                    headTitle: 'Update the course', coursesCurrent: 'current',
                    course: course
                });
            }
            else {
                res.status(404).render('course', {
                    headTitle: 'Courses', coursesCurrent: 'current',
                    course: null, msg: '404! No such course.'
                });
            }
        }
        catch (e) {
            console.log(e);
            res.status(500).render('course', {
                headTitle: 'Courses', coursesCurrent: 'current',
                course: null, msg: 'Server error!'
            });
        }
    },
    async getAndUpdateCoursePhoto(req, res) {
        try {
            const id = req.params.id;
            const course = await courseRepository.getCourseById(id);
            if (course) {
                res.status(200).render('updateCoursePhoto', {
                    headTitle: 'Update the course', coursesCurrent: 'current',
                    course: course
                });
            }
            else {
                res.status(404).render('course', {
                    headTitle: 'Courses', coursesCurrent: 'current',
                    course: null, msg: '404! No such course.'
                });
            }
        }
        catch (e) {
            console.log(e);
            res.status(500).render('course', {
                headTitle: 'Courses', coursesCurrent: 'current',
                course: null, msg: 'Server error!'
            });
        }
    },

    async updateCourse(req, res) {
        try {
            const id = req.params.id;
            const updatedInfo = req.body;
            const course = await courseRepository.updateCourse(id, updatedInfo);
            if (course) {
                res.status(303).redirect('/courses/' + id);
            }
            else {
                res.status(404).render('course', {
                    headTitle: 'Courses', coursesCurrent: 'current',
                    course: null, msg: '404! No such course.'
                });
            }
        }
        catch (e) {
            console.log(e);
            res.status(500).render('course', {
                headTitle: 'Courses', coursesCurrent: 'current',
                course: null, msg: 'Server error!'
            });
        }
    },

    async updateCoursePhoto(req, res) {
        const media = req.files.course_img;
        const id = req.params.id;
        try {
            let imgUpdate;
            if (media) {
                const img = await cloudinary.uploadRaw(media.data);
                imgUpdate = img.url;
            }
            else {
                imgUpdate = null;
            }
            const updatedScore = await courseRepository.updateCoursePhoto(id, imgUpdate);
            if (updatedScore) {
                res.status(303).redirect('/courses/' + id);
            }
            else {
                res.status(404).render('updateCourse', {
                    headTitle: 'Scores', scoresCurrent: 'current',
                    score: null, msg: '404! No such score.'
                });
            }
        }
        catch (e) {
            console.log(e);
            res.status(500).render('course', {
                headTitle: 'Courses', coursesCurrent: 'current',
                course: null, msg: 'Server error!'
            });
        }
    },

    async deleteCourseById(req, res) {
        try {
            const id = req.params.id;
            const course = await courseRepository.deleteCourse(id);
            if (course) {
                res.status(303).redirect('/courses');
            }
            else {
                res.status(404).render('course', {
                    headTitle: 'Courses', coursesCurrent: 'current',
                    course: null, msg: '404! No such course.'
                });
            }
        }
        catch (e) {
            console.log(e);
            res.status(500).render('course', {
                headTitle: 'Courses', coursesCurrent: 'current',
                course: null, msg: 'Server error!'
            });
        }
    }
};